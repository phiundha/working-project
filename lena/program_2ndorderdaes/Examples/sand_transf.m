%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function f = sand_transf(t,y,dy,ddy)

f= [ddy(1)-ddy(3)-2*y(2)-y(3)*(y(1)-y(3))
    ddy(2)+2*(y(1)-y(3))-y(3)*y(2)
    (y(1)-y(3))^2+y(2)^2-1];