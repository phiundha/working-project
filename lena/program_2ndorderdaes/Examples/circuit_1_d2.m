function f=circuit_1_d2(t,i,di)

L = 1/3;
C = 1.5;
R = 2;

f = [ 0 0 0 L
      0 1 0 0
      0 0 0 0
      0 0 0 0];