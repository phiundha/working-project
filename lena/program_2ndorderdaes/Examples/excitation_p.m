% excitation
%
function ud = excitation_p(t)

alpha0=-8.5238095238095e-2;
alpha(1)=1.2787390025886e-1;
alpha(2)=7.0383441249932e-4;
alpha(3)=3.1048302053411e-1;
alpha(4)=-3.3706894757361e-1;
alpha(5)=2.1524978898283e-1;
alpha(6)=-2.0360351352725e-1;
alpha(7)=3.0976190476190e-1;
alpha(8)=-2.5602498162985e-1;
alpha(9)=-6.4260459387813e-2;
alpha(10)=-6.0495499212637e-2;

gamma=1.9047619047619e-1;

beta(1)=3.2851543326605e-3;
beta(2)=-3.9247021690475e-1;
beta(3)=-2.1366627460535e-1;
beta(4)=1.2023541777852e-1;
beta(5)=6.2092591111477e-2;
beta(6)=4.2512072003927e-1;
beta(7)=-2.4454907830674e-1;
beta(8)=-4.0881660420336e-1;
beta(9)=9.2395636436012e-2;
beta(10)=7.4654599226469e-2;


sum=0;
sum2=0;
for i=1:10
    sum=sum+alpha(i)*cos(2*pi*i*t*gamma)+beta(i)*sin(2*pi*i*t*gamma);
    sum2=sum2+(-alpha(i)*2*pi*i*gamma*sin(2*pi*i*t*gamma)+beta(i)*2*pi*i*gamma*cos(2*pi*i*t*gamma));
end


ud=0.01*(1/2*alpha0+sum)*exp(-0.1*(t-7)^2)*(-0.2*(t-7))+0.01*(sum2)*exp(-0.1*(t-7)^2);