%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% exact solution of truck -> NOT KNOWN
%
% CALL  : ye = example_truck_exact(t)
%
%
% INPUT : t   - current time t\in[t0,t0+a]
%
% OUTPUT: ye   - exact solution at t
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
function ye = truck_exact(t)

ye=zeros(23,1);