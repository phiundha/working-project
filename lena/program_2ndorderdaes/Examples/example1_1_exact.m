function ye = example1_1_exact(t)

ye=[sin(t)
    cos(t)
    sin(t)*cos(t)
    cos(t)
    -sin(t)];