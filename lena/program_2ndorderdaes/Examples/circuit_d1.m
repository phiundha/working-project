function f=circuit_d1(t,i,di,ddi)

L = 1/3;
C = 1.5;

f = [ -1 0 0
      0 0 1
      1 1 1];
