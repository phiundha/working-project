function ye = Steffen_Exp_D_exact(t)

a = 10;

ye = -a*exp(-a*t)*[1;-1;2];