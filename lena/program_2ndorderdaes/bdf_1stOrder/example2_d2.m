function f = example2_d2(t,y,dy)

f= [1 0 0 0 0
    0 1 0 0 0
    0 0 0 y(4) y(5)
    0 0 0 0 0
    0 0 0 0 0];